package org.example.window;

import lombok.Data;

import javax.swing.*;
import java.awt.*;
import java.util.Arrays;
import java.util.List;

@Data
public class AddUpdatePanel {
    private JPanel mainPanel;
    private GridLayout mainGridLayout;
    private JPanel formPanel;
    private JPanel boutonPanel;
    private GridBagConstraints formBagConstraints;

    private JTextField nameTextField;
    private JTextField productIdTextField;
    private JTextField descriptionIdTextField;
    private JTextField quantityTextField;
    private JTextField unitPriceTextField;
    private List<String> labels = Arrays.asList("ProductID", "Name", "Description", "Quantity in hand", "Unit Price");

    private void createFieldForm() {
        formBagConstraints.weightx = 1;
        formBagConstraints.fill = GridBagConstraints.BOTH;

        productIdTextField = new JTextField();
        formBagConstraints.gridx = 1;
        formBagConstraints.gridy = 0;
        formPanel.add(productIdTextField, formBagConstraints);

        nameTextField = new JTextField("");
        formBagConstraints.gridx = 1;
        formBagConstraints.gridy = 1;
        formPanel.add(nameTextField, formBagConstraints);

        quantityTextField = new JTextField("");
        formBagConstraints.gridx = 2;
        formBagConstraints.gridy = 2;
        formPanel.add(quantityTextField, formBagConstraints);

        unitPriceTextField = new JTextField("");
        formBagConstraints.gridx = 2;
        formBagConstraints.gridy = 2;
        formPanel.add(unitPriceTextField, formBagConstraints);

//        adresseTextArea = new JTextArea("");
//        formBagConstraints.gridy = 2;
//        //formBagConstraints.weighty = 1;
//        formPanel.add(adresseTextArea, formBagConstraints);

//        calendar = new JDateChooser();
//        formBagConstraints.gridy = 3;
//        formPanel.add(calendar, formBagConstraints);

//        radioPanel = new JPanel();
      //  buttonGroup = new ButtonGroup();
//
//        fRadio = new JRadioButton("f");
//        mRadio = new JRadioButton("m");
//        radioPanel.setLayout(new FlowLayout());
//        buttonGroup.add(fRadio);
//        buttonGroup.add(mRadio);
//        radioPanel.add(fRadio);
//        radioPanel.add(mRadio);
        formBagConstraints.gridy = 4;
        formBagConstraints.fill = GridBagConstraints.CENTER;
//        formPanel.add(radioPanel, formBagConstraints);
    }

    public  AddUpdatePanel(JFrame frame) {

    }
}
