package org.example.window;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class MyStationeryPanel {
    private JPanel detailPanel;

    private GridBagLayout bagLayout;
    private GridBagConstraints bagConstraints;
    String columns[] = { "Item", "Quantity", "Cost" };
    String data[][] = new String[8][3];

    public MyStationeryPanel(){



        DefaultTableModel model = new DefaultTableModel(data, columns);
        JTable table = new JTable(model);
        table.setShowGrid(true);
        table.setShowVerticalLines(true);
        JScrollPane pane = new JScrollPane(table);
        JFrame f = new JFrame("My Stationery");
        JPanel panel1 = new JPanel();
        panel1.add(pane);
        f.add(panel1);
        f.setSize(550, 300);
        //f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);

    }
}
