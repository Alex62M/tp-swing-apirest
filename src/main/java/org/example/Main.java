package org.example;

import org.example.component.MenuPanel;
import org.example.window.AddUpdatePanel;

import javax.swing.*;
import java.awt.*;

public class Main {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Gestion Product");
        frame.setSize(new Dimension(600,600));
        new MenuPanel(frame);
        frame.setContentPane(new AddUpdatePane(frame).getMainPanel());
        frame.setVisible(true);

    }
}